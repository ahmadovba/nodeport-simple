FROM openjdk:11
ADD build/libs/nodeport-0.0.1-SNAPSHOT.jar nodeport-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","nodeport-0.0.1-SNAPSHOT.jar"]


