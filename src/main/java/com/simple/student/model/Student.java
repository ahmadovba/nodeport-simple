package com.simple.student.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long id;

    private String name;
//#   url: jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_NAME}
//           jdbc:mysql://${DB_HOST}/${DB_NAME}

    //automate
    /*
    1. clean
    2. build
    3. create image (which name it will use) -  dockerimage maybe?
    4. push

     */
}
