package com.simple.student.service;

import com.simple.student.model.Student;
import com.simple.student.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StudentService {
    private final StudentRepo repo;

    public Student addStudent(Student student) {
        return repo.save(student);
    }

    public List<Student> getStudents() {
       return repo.findAll();
    }
}

