package com.simple.student.web.rest;


import com.simple.student.model.Student;
import com.simple.student.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class Controller {
    private final StudentService studentService;


    @PostMapping("/student")
    public Student addStudent(String name) {
        Student student = Student.builder()
                .name(name).build();
        return studentService.addStudent(student);
    }

    @GetMapping("/students")
    public List<Student> getAllStudents() {

        return studentService.getStudents();
    }
}
